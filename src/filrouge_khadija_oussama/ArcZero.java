package filrouge_khadija_oussama;

public class ArcZero extends AbstractArc {

	/*
	 *  Implementing the constructor
	 *  The value of the arc must be zero
	 */
	public ArcZero(Place place, Transition transition) {
		super(0,place,transition);
	};
	
	/*
	 * Overriding the function because the arc's value should always be zero
	 */
	@Override
	public void setValeur(int newVal) {
		super.setValeur(0);
	}

	
	/*
	 * Allowing the CompareVisitor to do its job
	 * Returns true if the weight of the arc's place is zero
	 */
	@Override
	public boolean acceptCompare(ConcreteCompareVisitor visitor) {
		return visitor.visit(this);
	}

	
	/*
	 * Allowing the UpdateJetonsVisitor to do its job
	 */
	@Override
	public void acceptUpdateJetons(ConcreteUpdateJetonsVisitor visitor) {
		visitor.visit(this);
		
	}
}

	
