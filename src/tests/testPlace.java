package tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import filrouge_khadija_oussama.ArcEntrant;
import filrouge_khadija_oussama.ArcSortant;
import filrouge_khadija_oussama.ConcreteUpdateJetonsVisitor;
import filrouge_khadija_oussama.Place;
import filrouge_khadija_oussama.Transition;

public class testPlace {
	
	@Test
	@Order(1)
	void testConstructor() {
		Place p1 = new Place(2);
		assertEquals(2,p1.getJetons());
		
		Place p2 = new Place(-2);
		assertNotEquals(-2, p2.getJetons(),"Negative number of tokens");
	}
	
	@Test
	@Order(2)
	void testNewJetonEntrant() {
		Place p = new Place(2);
		int weight = p.getJetons();
		Transition t = new Transition();
		ArcEntrant a = new ArcEntrant(-1,p,t);
		t.addArcEntrant(a);
		ConcreteUpdateJetonsVisitor visitor = new ConcreteUpdateJetonsVisitor();
		a.acceptUpdateJetons(visitor);
		assertEquals(p.getJetons(),weight,"Arc's weight is negative");
		
		// Arc's place is different from the place on which we want to operate
		Place p1 = new Place(1);
		weight = p.getJetons();
		ArcEntrant a1 = new ArcEntrant(2,p1,t);
		t.addArcEntrant(a1);
		a1.acceptUpdateJetons(visitor);
		assertEquals(p.getJetons(), weight, "Arc's place is different from the place on which we operate");
		
		// Arc is not linked to the transition
		Place p2 = new Place(2);
		weight = p2.getJetons();
		ArcEntrant a2 = new ArcEntrant(2,p2,t);
		a2.acceptUpdateJetons(visitor);
		assertEquals(p.getJetons(), weight, "Arc is not linked to the transition");
		
		
		
		// All is good
		weight = p.getJetons();
		ArcEntrant a3 = new ArcEntrant(3,p,t);
		t.addArcEntrant(a3);
		a3.acceptUpdateJetons(visitor);
		assertEquals(weight+3, p.getJetons(),"All is good !");
	}
	
	@Test
	@Order(3)
	void testNewJetonSortant() {
		// Negative arc's weight
		Place p = new Place(2);
		int weight = p.getJetons();
		Transition t = new Transition();
		ArcSortant a = new ArcSortant(-1,p,t);
		t.addToAllArcsSortants(a);
		ConcreteUpdateJetonsVisitor visitor = new ConcreteUpdateJetonsVisitor();
		a.acceptUpdateJetons(visitor);
		assertEquals(p.getJetons(),weight,"Arc's weight is negative");
		
		// Arc's weight is bigger than the place's tokens number
		a = new ArcSortant(3,p,t);
		weight = p.getJetons();
		a.acceptUpdateJetons(visitor);
		assertEquals(p.getJetons(),weight,"Arc's weight is bigger than the place's tokens number");
		
		
		// Arc's place is different from the place on which we want to operate
		Place p1 = new Place(1);
		weight = p.getJetons();
		ArcSortant a1 = new ArcSortant(2,p1,t);
		t.addToAllArcsSortants(a1);
		a1.acceptUpdateJetons(visitor);
		assertEquals(p.getJetons(), weight, "Arc's place is different from the place on which we operate");
		
		// Arc is not linked to the transition
		Place p2 = new Place(2);
		weight = p2.getJetons();
		ArcSortant a2 = new ArcSortant(2,p2,t);
		a2.acceptUpdateJetons(visitor);
		assertEquals(p.getJetons(), weight, "Arc is not linked to the transition");
		
		// All is good
		p = new Place(5);
		weight = p.getJetons();
		ArcSortant a3 = new ArcSortant(3,p,t);
		t.addToAllArcsSortants(a3);
		a3.acceptUpdateJetons(visitor);
		assertEquals(weight-3, p.getJetons(),"All is good !");
	}

}
