package tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.LinkedList;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import filrouge_khadija_oussama.Arc;
import filrouge_khadija_oussama.ArcEntrant;
import filrouge_khadija_oussama.ArcSortant;
import filrouge_khadija_oussama.ArcVideur;
import filrouge_khadija_oussama.ArcZero;
import filrouge_khadija_oussama.Petri;
import filrouge_khadija_oussama.Place;
import filrouge_khadija_oussama.Transition;

public class testPetri {
	private Petri petri = new Petri();

	@Test
	@Order(1)
	void testConstructors() {
		Place p1 = new Place(4);
		Place p2 = new Place(0);
		Place p3 = new Place(2);
		Transition t1 = new Transition();
		Transition t2 = new Transition();
	    ArcEntrant ae1 = new ArcEntrant(1,p1,t1);
	    ArcEntrant ae2 = new ArcEntrant(2,p3,t1);
	    ArcEntrant ae3 = new ArcEntrant(2,p2,t2);
		t1.addArcEntrant(ae1);
		t1.addArcEntrant(ae2);
		t2.addArcEntrant(ae3);
		ArcSortant as1 = new ArcSortant(1,p2,t1);
		ArcSortant as2 = new ArcSortant(1,p3,t1);
		ArcSortant as3 = new ArcSortant(1,p2,t2);
		t1.addToAllArcsSortants(as1);
		t1.addToAllArcsSortants(as2);
		t2.addToAllArcsSortants(as3);
		LinkedList<Transition> listeTransitions = new LinkedList<Transition>();
		listeTransitions.add(t1);
		LinkedList<Place> listePlaces = new LinkedList<Place>();
		listePlaces.add(p1);
		listePlaces.add(p2);
		LinkedList<ArcEntrant> listeArcEntrant = new LinkedList<ArcEntrant>();
		listeArcEntrant.add(ae1);
		LinkedList<Arc> listeArcSortant = new LinkedList<Arc>();
		listeArcSortant.add(as1);
		Petri petri = new Petri(listeTransitions,listeArcEntrant, listeArcSortant,listePlaces);
		
		//ArcEntrant contains a place that doesn't exist in the list of places
		listeArcEntrant.add(ae2);
		Petri petri1 = new Petri(listeTransitions,listeArcEntrant,listeArcSortant,listePlaces);
		assertEquals(0, petri1.getArcsEntrants().size(), "ArcEntrant doesn't contain a place that doesn't exist in the list of places");
		listeArcEntrant.remove(ae2);
		
		//ArcSortant contains a place that doesn't exist in the list of places
		listeArcSortant.add(as2);
		Petri petri2 = new Petri(listeTransitions,listeArcEntrant,listeArcSortant,listePlaces);
		assertEquals(0, petri2.getAllArcsSortants().size(),"ArcSortant doesn't contain a place that doesn't exist in the list of places");
		listeArcSortant.remove(as2);
		
		//ArcEntrant contains a transition that doesn't exist in the list of transitions
		listeArcEntrant.add(ae3);
		Petri petri3 = new Petri(listeTransitions,listeArcEntrant,listeArcSortant,listePlaces);
		assertEquals(0, petri3.getArcsEntrants().size(), "ArcEntrant doesn't contain a transition that doesn't exist in the list of transitions");
		listeArcEntrant.remove(ae3);
		
		//ArcSortant contains a transition that doesn't exist in the list of transitions
		listeArcSortant.add(as3);
		Petri petri4 = new Petri(listeTransitions,listeArcEntrant,listeArcSortant,listePlaces);
		assertEquals(0, petri4.getAllArcsSortants().size(),"ArcSortant doesn't contain a transition that doesn't exist in the list of transitions");
		listeArcSortant.remove(as3);
		
		//ArcEntrants are unique
		ArcEntrant ae4 = new ArcEntrant(2,p1,t1);
		listeArcEntrant.add(ae4);
		Petri petri5 = new Petri(listeTransitions,listeArcEntrant,listeArcSortant,listePlaces);
		assertEquals(0, petri5.getArcsEntrants().size(), "ArcsEntrants elements are unique");
		listeArcEntrant.remove(ae4);
		
		//ArcSortants are unique
		ArcSortant as4 = new ArcSortant(2,p2,t1);
		listeArcSortant.add(as4);
		Petri petri6 = new Petri(listeTransitions,listeArcEntrant,listeArcSortant,listePlaces);
		assertEquals(0, petri6.getAllArcsSortants().size(),"AllArcsSortants elements are unique");
		listeArcSortant.remove(as4);
		
		// Transitions are connected with the corresponding arcs
		Place p4 = new Place(6);
		Transition t3 = new Transition();
		ArcEntrant ae5 = new ArcEntrant (1,p4,t3);
		listeArcEntrant.add(ae5);
		listePlaces.add(p4);
		listeTransitions.add(t3);
		t3.addArcEntrant(ae5);
		Petri petri7 = new Petri(listeTransitions,listeArcEntrant,listeArcSortant,listePlaces);
		assertNotNull(petri7.getArcsEntrants());
		assertNotNull(petri7.getArcsEntrants());
		assertNotNull(petri7.getPlaces());
		assertNotNull(petri7.getTransitions());
		assertNotEquals(0,petri7.getArcsEntrants().size(),"All is good!");
		assertNotEquals(0, petri7.getArcsEntrants().size(),"All is good!");
		assertNotEquals(0, petri7.getTransitions().size(),"All is good!");
		assertNotEquals(0, petri7.getPlaces().size(),"All is good");
		
		// Testing the second constructor
		Petri p = new Petri();
		assertNotNull(p.getArcsEntrants());
		assertNotNull(p.getArcsEntrants());
		assertNotNull(p.getPlaces());
		assertNotNull(p.getTransitions());
		assertEquals(p.getArcsEntrants().size(),0);
		assertEquals(p.getArcsEntrants().size(),0);
		assertEquals(p.getPlaces().size(),0);
		assertEquals(p.getTransitions().size(),0);
	}
	
	@Test
	@Order(2)
	void testTransition() {
		// All is good
		Transition t = new Transition();
		int size = petri.getTransitions().size();
		petri.addTransition(t);
		assertEquals(size + 1, petri.getTransitions().size(),"All is good!");
	}
	
	@Test
	@Order(3)
	void testAddPlace() {
		// All is goood
		Place p1 = new Place(2);
		int size = petri.getPlaces().size();
		petri.addPlace(p1);
		assertEquals(size+1, petri.getPlaces().size(),"All is good!");
		
		// Negative number of tokens
		Place p2 = new Place(-2);
		int size1 = petri.getPlaces().size();
		petri.addPlace(p2);
		assertEquals(size1, petri.getPlaces().size(),"Negative number of tokens");
	}
	
	
	@Test
	@Order(4)
	void testAddArcEntrant() {
		Place p1 = new Place(4);
		Place p2 = new Place(0);
		Transition t1 = new Transition();
	    ArcEntrant ae1 = new ArcEntrant(1,p1,t1);
		t1.addArcEntrant(ae1);
		ArcSortant as1 = new ArcSortant(1,p2,t1);
		t1.addToAllArcsSortants(as1);
		LinkedList<Transition> listeTransitions = new LinkedList<Transition>();
		listeTransitions.add(t1);
		LinkedList<Place> listePlaces = new LinkedList<Place>();
		listePlaces.add(p1);
		listePlaces.add(p2);
		LinkedList<ArcEntrant> listeArcEntrant = new LinkedList<ArcEntrant>();
		listeArcEntrant.add(ae1);
		LinkedList<Arc> listeArcSortant = new LinkedList<Arc>();
		listeArcSortant.add(as1);
		Petri petri = new Petri(listeTransitions,listeArcEntrant,listeArcSortant,listePlaces);
		
		//Arc Entrant already exists
		ArcEntrant ae2 = new ArcEntrant(2,p1,t1);
		int size_ae = petri.getArcsEntrants().size();
		petri.addArcEntrant(ae2);
		assertEquals(size_ae, petri.getArcsEntrants().size(), "This arc already exists");
		
		// Arc Entrant does not exist and is not linked to its transition but all is good thanks to the observer
		ArcEntrant ae3 = new ArcEntrant(1,p2,t1);
		size_ae = petri.getArcsEntrants().size();
		petri.addArcEntrant(ae3);
		assertEquals(size_ae+1, petri.getArcsEntrants().size(), "All is good thanks to the observer !");
		petri.removeArcEntrant(ae3);
		
	}
	
	@Test
	@Order(5)
	void testAddArcSortant() {
		Place p1 = new Place(4);
		Place p2 = new Place(0);
		Transition t1 = new Transition();
	    ArcEntrant ae1 = new ArcEntrant(1,p1,t1);
		t1.addArcEntrant(ae1);
		ArcSortant as1 = new ArcSortant(1,p2,t1);
		t1.addToAllArcsSortants(as1);
		LinkedList<Transition> listeTransitions = new LinkedList<Transition>();
		listeTransitions.add(t1);
		LinkedList<Place> listePlaces = new LinkedList<Place>();
		listePlaces.add(p1);
		listePlaces.add(p2);
		LinkedList<ArcEntrant> listeArcEntrant = new LinkedList<ArcEntrant>();
		listeArcEntrant.add(ae1);
		LinkedList<Arc> listeArcSortant = new LinkedList<Arc>();
		listeArcSortant.add(as1);
		Petri petri = new Petri(listeTransitions,listeArcEntrant,listeArcSortant,listePlaces);
		
		//Arc Sortant already exists
		ArcSortant as2 = new ArcSortant(2,p2,t1);
		int size_as = petri.getAllArcsSortants().size();
		petri.addToAllArcsSortants(as2);
		assertEquals(size_as, petri.getAllArcsSortants().size(), "This arc already exists");
		
		// Arc Sortant does not exist and is not linked to its transition but it's done automatically thanks to the observer
		ArcSortant as3 = new ArcSortant(1,p1,t1);
		size_as = petri.getAllArcsSortants().size();
		petri.addToAllArcsSortants(as3);
		assertEquals(size_as+1, petri.getAllArcsSortants().size(),"Observer works well !");
		petri.removeArcSortant(as3);
		
	}
	
	@Test
	@Order(6)
	void testAddArcVideur() {
		Place p1 = new Place(4);
		Place p2 = new Place(1);
		Transition t1 = new Transition();
	    ArcEntrant ae1 = new ArcEntrant(1,p1,t1);
		t1.addArcEntrant(ae1);
		ArcVideur av1 = new ArcVideur(p2,t1);
		t1.addToAllArcsSortants(av1);
		LinkedList<Transition> listeTransitions = new LinkedList<Transition>();
		listeTransitions.add(t1);
		LinkedList<Place> listePlaces = new LinkedList<Place>();
		listePlaces.add(p1);
		listePlaces.add(p2);
		LinkedList<ArcEntrant> listeArcEntrant = new LinkedList<ArcEntrant>();
		listeArcEntrant.add(ae1);
		LinkedList<Arc> listeArcSortant = new LinkedList<Arc>();
		listeArcSortant.add(av1);
		Petri petri = new Petri(listeTransitions,listeArcEntrant,listeArcSortant,listePlaces);
		
		//Arc Videur already exists
		ArcVideur av2 = new ArcVideur(p2,t1);
		int size_as = petri.getAllArcsSortants().size();
		petri.addToAllArcsSortants(av2);
		assertEquals(size_as, petri.getAllArcsSortants().size(), "This arc already exists");
		
		// Arc Videur does not exist and is not linked to its transition but it's done automatically thanks to the observer
		ArcVideur av3 = new ArcVideur(p1,t1);
		size_as = petri.getAllArcsSortants().size();
		petri.addToAllArcsSortants(av3);
		assertEquals(size_as+1, petri.getAllArcsSortants().size(),"Observer works well !");
		petri.removeArcSortant(av3);
		
	}
	
	@Test
	@Order(7)
	void testAddArcZero() {
		Place p1 = new Place(4);
		Place p2 = new Place(0);
		Transition t1 = new Transition();
	    ArcEntrant ae1 = new ArcEntrant(1,p1,t1);
		t1.addArcEntrant(ae1);
		ArcZero az1 = new ArcZero(p2,t1);
		t1.addToAllArcsSortants(az1);
		LinkedList<Transition> listeTransitions = new LinkedList<Transition>();
		listeTransitions.add(t1);
		LinkedList<Place> listePlaces = new LinkedList<Place>();
		listePlaces.add(p1);
		listePlaces.add(p2);
		LinkedList<ArcEntrant> listeArcEntrant = new LinkedList<ArcEntrant>();
		listeArcEntrant.add(ae1);
		LinkedList<Arc> listeArcSortant = new LinkedList<Arc>();
		listeArcSortant.add(az1);
		Petri petri = new Petri(listeTransitions,listeArcEntrant,listeArcSortant,listePlaces);
		
		//Arc Zero already exists
		ArcZero az2 = new ArcZero(p2,t1);
		int size_as = petri.getAllArcsSortants().size();
		petri.addToAllArcsSortants(az2);
		assertEquals(size_as, petri.getAllArcsSortants().size(), "This arc already exists");
		
		// Arc Zero does not exist and is not linked to its transition but it's done automatically thanks to the observer
		ArcZero az3 = new ArcZero(p1,t1);
		size_as = petri.getAllArcsSortants().size();
		petri.addToAllArcsSortants(az3);
		assertEquals(size_as+1, petri.getAllArcsSortants().size(),"Observer works well !");
		petri.removeArcSortant(az3);
		
	}
	
	@Test
	@Order(8)
	void testRemoveTransition() {
		Place place1 = new Place(1);
		Place place2 = new Place(2);
		Transition t1 = new Transition();
		ArcEntrant ae1 = new ArcEntrant(1,place1,t1);
		ArcSortant as1 = new ArcSortant(2,place2,t1);
		t1.addArcEntrant(ae1);
		t1.addToAllArcsSortants(as1);
		LinkedList<Transition> lT = new LinkedList<Transition>();
		lT.add(t1);
		LinkedList<Place> lP = new LinkedList<Place>();
		lP.add(place2);
		lP.add(place1);
		LinkedList<ArcEntrant> lAE = new LinkedList<ArcEntrant>();
		lAE.add(ae1);
		LinkedList<Arc> lAS = new LinkedList<Arc>();
		lAS.add(as1);
		Petri petri = new Petri(lT,lAE,lAS,lP);
		petri.removeTransition(t1);
		assertFalse(petri.exists(lT, t1));
		for (ArcEntrant ae: lAE) {
			if (ae.getTransition().equals(t1)){
				assertFalse(petri.exists(lAE, ae));
			}
		}
		for (Arc as: lAS) {
			if (as.getTransition().equals(t1)) {
				assertFalse(petri.exists(lAS, as));
			}
		}
	}
	

	
	@Test
	@Order(9)
	void testRemoveArcZero() {
		Place p1 = new Place(4);
		Place p2 = new Place(0);
		Transition t1 = new Transition();
	    ArcEntrant ae1 = new ArcEntrant(1,p1,t1);
		t1.addArcEntrant(ae1);
		ArcZero az1 = new ArcZero(p2,t1);
		t1.addToAllArcsSortants(az1);
		LinkedList<Transition> listeTransitions = new LinkedList<Transition>();
		listeTransitions.add(t1);
		LinkedList<Place> listePlaces = new LinkedList<Place>();
		listePlaces.add(p1);
		listePlaces.add(p2);
		LinkedList<ArcEntrant> listeArcEntrant = new LinkedList<ArcEntrant>();
		listeArcEntrant.add(ae1);
		LinkedList<Arc> listeArcSortant = new LinkedList<Arc>();
		listeArcSortant.add(az1);
		Petri petri = new Petri(listeTransitions,listeArcEntrant,listeArcSortant,listePlaces);
		int size = petri.getAllArcsSortants().size();
		petri.removeArcSortant(az1);
		assertEquals(size-1, petri.getAllArcsSortants().size());
	}
	
	
	@Test
	@Order(10)
	void testRemoveArcVideur() {
		Place p1 = new Place(4);
		Place p2 = new Place(1);
		Transition t1 = new Transition();
	    ArcEntrant ae1 = new ArcEntrant(1,p1,t1);
		t1.addArcEntrant(ae1);
		ArcVideur av1 = new ArcVideur(p2,t1);
		t1.addToAllArcsSortants(av1);
		LinkedList<Transition> listeTransitions = new LinkedList<Transition>();
		listeTransitions.add(t1);
		LinkedList<Place> listePlaces = new LinkedList<Place>();
		listePlaces.add(p1);
		listePlaces.add(p2);
		LinkedList<ArcEntrant> listeArcEntrant = new LinkedList<ArcEntrant>();
		listeArcEntrant.add(ae1);
		LinkedList<Arc> listeArcSortant = new LinkedList<Arc>();
		listeArcSortant.add(av1);
		Petri petri = new Petri(listeTransitions,listeArcEntrant,listeArcSortant,listePlaces);
		int size = petri.getAllArcsSortants().size();
		petri.removeArcSortant(av1);
		assertEquals(size-1, petri.getAllArcsSortants().size());
	}
	
	
	@Test
	@Order(11)
	void testRemovePlace() {
		Place place1 = new Place(1);
		Transition t1 = new Transition();
		Transition t2 = new Transition();
		ArcEntrant ae1 = new ArcEntrant(1,place1,t1);
		ArcEntrant ae2 = new ArcEntrant(2,place1,t2);
		t1.addArcEntrant(ae1);
		t1.addArcEntrant(ae2);
		LinkedList<Transition> lT = new LinkedList<Transition>();
		lT.add(t1);
		lT.add(t2);
		LinkedList<Place> lP = new LinkedList<Place>();
		lP.add(place1);
		LinkedList<ArcEntrant> lAE = new LinkedList<ArcEntrant>();
		lAE.add(ae1);
		lAE.add(ae2);
		LinkedList<Arc> lAS = new LinkedList<Arc>();
		Petri petri = new Petri(lT,lAE,lAS,lP);
		petri.removePlace(place1);
		assertFalse(petri.exists(petri.getPlaces(), place1));
		assertEquals(0, petri.getArcsEntrants().size());
	}
	
	@Test
	@Order(12)
	void testGenererInt() {
		Petri petri = new Petri();
		int i = petri.genererInt(0, 2);
		assertTrue(i>=0);
		assertTrue(2-i >= 0);
	}
	
	
	@Test
	@Order(13)
	void testChoixTransition() {
		Transition t1 = new Transition();
		Transition t2 = new Transition();
		LinkedList<Transition> listeTransition = new LinkedList<Transition>();
		listeTransition.add(t2);
		listeTransition.add(t1);
		Petri petri = new Petri();
		Transition t = petri.choixTransition(listeTransition);
		assertTrue(t.equals(t1) || t.equals(t2));
	}
	
	@Test
	@Order(14)
	void testTirer() {
		Place p1 = new Place(2);
		Transition t1 = new Transition();
		Place p2 = new Place(3);
		ArcSortant as = new ArcSortant(1,p1,t1);
		t1.addToAllArcsSortants(as);
		ArcEntrant ae = new ArcEntrant(2,p2,t1);
		t1.addArcEntrant(ae);
		LinkedList<Place> listePlace = new LinkedList<Place>();
		listePlace.add(p2);
		listePlace.add(p1);
		LinkedList<Transition> listeTransition = new LinkedList<Transition>();
		listeTransition.add(t1);
		LinkedList<ArcEntrant> listeArcEntrant = new LinkedList<ArcEntrant>();
		listeArcEntrant.add(ae);
		LinkedList<Arc> listeArcSortant = new LinkedList<Arc>();
		listeArcSortant.add(as);
		Petri petri = new Petri(listeTransition,listeArcEntrant,listeArcSortant,listePlace);
		petri.step();
		assertEquals(p1.getJetons(),1);
		assertEquals(p2.getJetons(),5);
	}
	
	
	@Test
	@Order(15)
	// Testing the display in the console
	void testAfficher() {
		// Creation of RdP
		Petri petri = new Petri();
				
		// Creation of places
		Place p1 = new Place(4);
		Place p2 = new Place(0);
		petri.addPlace(p1);
		petri.addPlace(p2);
				
		// Creation of transitions
		Transition t1 = new Transition();
		petri.addTransition(t1);
				
		// Creation of "Arcs Entrants"
		ArcEntrant ae1 = new ArcEntrant(-1,p2,t1);
		petri.addArcEntrant(ae1);
		t1.addArcEntrant(ae1);
				
		// Creation of "Arcs Sortants"
		ArcSortant as1 = new ArcSortant(1,p1,t1);
		petri.addToAllArcsSortants(as1);
		t1.addToAllArcsSortants(as1);
		
		//Display of RdP
		LinkedList<Transition> lT = new LinkedList<Transition>();
		lT.add(t1);
		LinkedList<Place> lP = new LinkedList<Place>();
		lP.add(p2);
		lP.add(p1);
		LinkedList<ArcEntrant> lAE = new LinkedList<ArcEntrant>();
		lAE.add(ae1);
		LinkedList<ArcSortant> lAS = new LinkedList<ArcSortant>();
		lAS.add(as1);
		petri.afficher(lP, lT, lAE, lAS);
		
	}
	
	
	
}
