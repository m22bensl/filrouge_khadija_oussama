package filrouge_khadija_oussama;

import java.util.LinkedList;

public class Transition implements Subscriber{
	
	//The class's variables
	private LinkedList<ArcEntrant> arcsEntrants;
	private LinkedList<Arc> allArcsSortants;
	
	/*
	 * Implementing the constructor
	 * Creating void lists that will be filled once an arc is created thanks to the Observer design pattern
	 */
	public Transition() {
		this.arcsEntrants = new LinkedList<ArcEntrant>();
		this.allArcsSortants = new LinkedList<Arc>();
	}
	
	
	
	// Adding elements to the lists
	/*
	 * Adding an arc of type arcEntrant to the list arcsEntrants
	 */
	public void addArcEntrant(ArcEntrant ae) {
		this.arcsEntrants.add(ae);
	}
	

	/*
	 * Adding an arc of type ArcSortant to the list allArcsSortants
	 */
	public void addToAllArcsSortants(ArcSortant arc) {
		this.allArcsSortants.add(arc);
	}
	
	
	/*
	 * The overloading allows us to add elements of different types to the same list
	 * Adding an arc of type ArcZero to the list
	 */
	public void addToAllArcsSortants(ArcZero arc) {
		this.allArcsSortants.add(arc);
	}
	
	
	/*
	 * Adding an arc of type ArcVideur to the list
	 * Overloading
	 */
	public void addToAllArcsSortants(ArcVideur arc) {
		this.allArcsSortants.add(arc);
	}
	
	
	// Implementing the getters
	/*
	 * This getter will allow us to update the places of these arcs once a transition is picked
	 */
	public LinkedList<ArcEntrant> getArcsEntrants() {
		return this.arcsEntrants;
	}
	
	/*
	 * This getter will allow us to test if the arcs are activated
	 */
	public LinkedList<Arc> getAllArcsSortants(){
		return this.allArcsSortants;
	}
	
	
	
	/*
	 * Useful for some tests
	 */
	@Override
	public boolean equals(Object o) {
		return o instanceof Transition &&
				((Transition)o).getArcsEntrants().equals(this.getArcsEntrants()) &&
				((Transition)o).getAllArcsSortants().equals(this.getAllArcsSortants());
	}
	
	
	/*
	 * Checks whether a transition can be picked or not
	 * Using the CompareVisitor visitor for each arc
	 */
	public boolean estTirable() {
		ConcreteCompareVisitor visitor = new ConcreteCompareVisitor();
		int i = 0;
		LinkedList<Arc> listAllArcsSortants = this.getAllArcsSortants();
		int size = listAllArcsSortants.size();
		boolean test = true;
		while(test == true && i<size) {
			test = listAllArcsSortants.get(i).acceptCompare(visitor);
			i++;
		}
		return test;
	}

	
	/*
	 * Picking a transition if the conditions are verified
	 * Updating the places' weights
	 */
	public void tirer() {
		ConcreteUpdateJetonsVisitor visitor = new ConcreteUpdateJetonsVisitor();
		LinkedList<Arc> listAllArcsSortants = this.getAllArcsSortants();
		int size = listAllArcsSortants.size();
		for (int i=0; i< size ;i++) {
			Arc arc = listAllArcsSortants.get(i);
			arc.acceptUpdateJetons(visitor);
		}
		
		LinkedList<ArcEntrant> listArcsEntrants = this.getArcsEntrants(); 
		size = listArcsEntrants.size();
		for (int i = 0 ;i< size; i++ ) {
			ArcEntrant arcE = listArcsEntrants.get(i);
			arcE.acceptUpdateJetons(visitor);
		}
	}
	
	
	/*
	 * Useful to print the elements of the RdP
	 */
	@Override
	public String toString() {
		String resultat = "-Transition avec "+this.getArcsEntrants().size()+" arc(s) entrant(s) et "+
	                       this.getAllArcsSortants().size() +" arc(s) sortant(s). ";
		resultat += "Les arcs entrants vers cette transition sont: "+"\n";
		for (ArcEntrant ae:this.getArcsEntrants()) {
			if (ae.getValeur()>=0) {
			resultat += "    *" + ae.toString() + "\n";
			} else {
				int i = ae.getValeur()+1;
				resultat += "    *" + "Arc de poids négatif " + i + " ce qui est impossible" + "\n";
			}
		}
		resultat += "Les arcs sortants de cette transition sont: "+"\n";
		for (Arc as: this.getAllArcsSortants()) {
			if (as.getValeur()>=0) {
				resultat += "    *" + as.toString() + "\n";
			} else {
				// We add +1 because of the trick we did to pass the tests in Arc
				int i = as.getValeur()+1;
				resultat += "    *" + "Arc de poids négatif " + i + " ce qui est impossible" + "\n";
			}
		}
		return resultat;
	}

	
	/*
	 * Implementing the Observer design pattern
	 * Once a change occurs in the Petri class ( a new arc created ) this function is activated
	 * Adding the created arc to the appropriate list
	 */
	@Override
	public void update(Arc arc) {
		if (arc instanceof ArcEntrant) {
			this.addArcEntrant((ArcEntrant)arc);
		} else if (arc instanceof ArcSortant) {
			this.addToAllArcsSortants((ArcSortant)arc);
		} else if (arc instanceof ArcZero) {
			this.addToAllArcsSortants((ArcZero)arc);
		} else if (arc instanceof ArcVideur) {
			this.addToAllArcsSortants((ArcVideur)arc);
		}
		
	}
}
