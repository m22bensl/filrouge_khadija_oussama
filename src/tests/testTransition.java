
package tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.LinkedList;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import filrouge_khadija_oussama.Transition;
import filrouge_khadija_oussama.Place; 
import filrouge_khadija_oussama.ArcSortant;
import filrouge_khadija_oussama.Arc;
import filrouge_khadija_oussama.ArcEntrant; 
import filrouge_khadija_oussama.ArcVideur; 
import filrouge_khadija_oussama.ArcZero;
import filrouge_khadija_oussama.Petri; 


public class testTransition {
	
	@Test
	@Order(1)
	void testConstructor() {
		Transition t = new Transition();
		assertNotNull(t.getArcsEntrants());
		assertNotNull(t.getAllArcsSortants());
		assertEquals(t.getArcsEntrants().size(),0);
		assertEquals(t.getAllArcsSortants().size(),0);
	}
	
	@Test
	@Order(2)
	void testRI() {
		Transition transi = new Transition();
		if(transi.estTirable()) transi.tirer();
		assertEquals(transi,transi);	
	}
	
	@Test
	@Order(3)
	//ArcSortant test : Transition in not activated 
	void testRD0() {
		Transition transi = new Transition();
		Place place=new Place(0);
		ArcSortant arcSortant= new ArcSortant(1, place,transi);
		transi.addToAllArcsSortants(arcSortant);
		if(transi.estTirable()) transi.tirer();
		assertEquals(place.getJetons(),0);	
	}
	
	@Test
	@Order(4)
	//ArcSortant test : ArcSortant not linked to the activated transition
	void testRD5() {
		Transition transi = new Transition();
		Transition transi2 = new Transition();
		Place place=new Place(2);
		ArcSortant arcSortant= new ArcSortant(1, place,transi2);
		transi2.addToAllArcsSortants(arcSortant);
		if(transi.estTirable()) transi.tirer();
		assertEquals(place.getJetons(),2);
	}
	
	@Test
	@Order(5)
	void testRD1() {
		Transition transi = new Transition();
		Place place=new Place(2);
		ArcSortant arcSortant= new ArcSortant(1, place,transi);
		transi.addToAllArcsSortants(arcSortant);
		if(transi.estTirable()) transi.tirer();
		assertEquals(place.getJetons(),1);	
	}
	
	@Test
	@Order(6)
	void testRD2() {
		Transition transi = new Transition();
		Place place=new Place(5);
		ArcSortant arcSortant= new ArcSortant(3, place,transi);
		transi.addToAllArcsSortants(arcSortant);
		if(transi.estTirable()) transi.tirer();
		assertEquals(place.getJetons(),2);	
	}
	
	@Test
	@Order(7)
	void testRD3() {
		Transition transi = new Transition();
		Place place=new Place(2);
		ArcSortant arcSortant= new ArcSortant(3, place,transi);
		transi.addToAllArcsSortants(arcSortant);
		if(transi.estTirable()) transi.tirer();
		assertEquals(place.getJetons(),2);	
	}
	
	@Test
	@Order(8)
	void testRD4() {
		Transition transi = new Transition();
		Place place=new Place(2);
		ArcSortant arcSortant= new ArcSortant(2, place,transi);
		transi.addToAllArcsSortants(arcSortant);
		if(transi.estTirable()) { 
			transi.tirer();}
		assertEquals(place.getJetons(),0);	
	}
	
	@Test
	@Order(9)
	void testRG0() {
		Transition transi = new Transition();
		Place place=new Place(0);
		ArcEntrant arcEntrant= new ArcEntrant(1, place,transi);
		transi.addArcEntrant(arcEntrant);
		if (transi.estTirable()) transi.tirer();
		assertEquals(place.getJetons(),1);	
	}
	
	@Test
	@Order(10)
	void testRG1() {
		Transition transi = new Transition();
		Place place=new Place(0);
		ArcEntrant arcEntrant= new ArcEntrant(2, place,transi);
		transi.addArcEntrant(arcEntrant);
		if (transi.estTirable()) transi.tirer();
		assertEquals(place.getJetons(),2);	
	}
	
	@Test
	@Order(11)
	void testRM1() {
		Transition transi = new Transition();
		Place placeEntrante=new Place(0);
		Place placeSortante=new Place(1);
		ArcEntrant arcEntrant= new ArcEntrant(1, placeEntrante,transi);
		ArcSortant arcSortant = new ArcSortant(1, placeSortante, transi);
		transi.addArcEntrant(arcEntrant);
		transi.addToAllArcsSortants(arcSortant);
		if(transi.estTirable()) transi.tirer();
		assertEquals(placeSortante.getJetons(),0);
		assertEquals(placeEntrante.getJetons(),1);
		
	}
	
	@Test
	@Order(12)
	void testRM2() {
		Transition transi = new Transition();
		Place placeEntrante=new Place(1);
		Place placeSortante=new Place(1);
		ArcEntrant arcEntrant= new ArcEntrant(1, placeEntrante,transi);
		ArcSortant arcSortant = new ArcSortant(1, placeSortante, transi);
		transi.addArcEntrant(arcEntrant);
		transi.addToAllArcsSortants(arcSortant);
		if(transi.estTirable()) transi.tirer();
		assertEquals(placeSortante.getJetons(),0);
		assertEquals(placeEntrante.getJetons(),2);
		
	}
	
	@Test
	@Order(13)
	void testRM3() {
		Transition transi = new Transition();
		Place placeEntrante=new Place(0);
		Place placeSortante=new Place(3);
		ArcEntrant arcEntrant= new ArcEntrant(1, placeEntrante,transi);
		ArcSortant arcSortant = new ArcSortant(2, placeSortante, transi);
		transi.addArcEntrant(arcEntrant);
		transi.addToAllArcsSortants(arcSortant);
		if(transi.estTirable()) transi.tirer();
		assertEquals(placeSortante.getJetons(),1);
		assertEquals(placeEntrante.getJetons(),1);	 
	}
	
	@Test
	@Order(14)
	void testRM4() {
		Transition transi = new Transition();
		Place placeEntrante=new Place(0);
		Place placeSortante=new Place(1);
		ArcEntrant arcEntrant= new ArcEntrant(2, placeEntrante,transi);
		ArcSortant arcSortant = new ArcSortant(1, placeSortante, transi);
		transi.addArcEntrant(arcEntrant);
		transi.addToAllArcsSortants(arcSortant);
		if(transi.estTirable()) {transi.tirer();}
		assertEquals(placeSortante.getJetons(),0);
		assertEquals(placeEntrante.getJetons(),2);	 
	}
	
	@Test
	@Order(15)
	void testRM5() {
		Transition transi = new Transition();
		Place placeEntrante=new Place(0);
		Place placeSortante=new Place(0);
		ArcEntrant arcEntrant= new ArcEntrant(1, placeEntrante,transi);
		ArcSortant arcSortant = new ArcSortant(1, placeSortante, transi);
		transi.addArcEntrant(arcEntrant);
		transi.addToAllArcsSortants(arcSortant);
		assertEquals(transi.estTirable(),false);
	}
	
	@Test
	@Order(16)
	void testRM6() {
		Transition transi = new Transition();
		Place placeEntrante=new Place(1);
		Place placeSortante=new Place(0);
		ArcEntrant arcEntrant= new ArcEntrant(1, placeEntrante,transi);
		ArcSortant arcSortant = new ArcSortant(1, placeSortante, transi);
		transi.addArcEntrant(arcEntrant);
		transi.addToAllArcsSortants(arcSortant);
		assertEquals(transi.estTirable(),false);
	}
	
	@Test
	@Order(17)
	void testTM1() {
		Transition transi = new Transition();
		Place placeSortante1=new Place(2);
		Place placeSortante2=new Place(1);
		ArcSortant arcSortant1 = new ArcSortant(1, placeSortante1, transi);
		ArcSortant arcSortant2 = new ArcSortant(1, placeSortante2, transi);
		transi.addToAllArcsSortants(arcSortant1);
		transi.addToAllArcsSortants(arcSortant2);
		if(transi.estTirable()) {
			transi.tirer();
		}
		assertEquals(placeSortante1.getJetons(),1);
		assertEquals(placeSortante2.getJetons(),0);
	}

	@Test
	@Order(18)
	void testTM2() {
		Transition transi = new Transition();
		Place placeSortante1=new Place(0);
		Place placeSortante2=new Place(0);
		ArcSortant arcSortant1 = new ArcSortant(1, placeSortante1, transi);
		ArcSortant arcSortant2 = new ArcSortant(1, placeSortante2, transi);
		transi.addToAllArcsSortants(arcSortant1);
		transi.addToAllArcsSortants(arcSortant2);
		assertEquals(transi.estTirable(),false);
	}
	
	@Test
	@Order(19)
	void testTM3() {
		Transition transi = new Transition();
		Place placeSortante1=new Place(2);
		Place placeSortante2=new Place(0);
		ArcSortant arcSortant1 = new ArcSortant(1, placeSortante1, transi);
		ArcSortant arcSortant2 = new ArcSortant(1, placeSortante2, transi);
		transi.addToAllArcsSortants(arcSortant1);
		transi.addToAllArcsSortants(arcSortant2);
		assertEquals(transi.estTirable(),false);
	}
	
	@Test
	@Order(20)
	void testTM4() {
		Transition transi = new Transition();
		Place placeSortante1=new Place(5);
		Place placeSortante2=new Place(2);
		ArcSortant arcSortant1 = new ArcSortant(3, placeSortante1, transi);
		ArcSortant arcSortant2 = new ArcSortant(2, placeSortante2, transi);
		transi.addToAllArcsSortants(arcSortant1);
		transi.addToAllArcsSortants(arcSortant2);
		if(transi.estTirable()) { 
			transi.tirer();
		}
		assertEquals(placeSortante1.getJetons(),2);
		assertEquals(placeSortante2.getJetons(),0);
	}
	
	@Test
	@Order(21)
	void testTM5() {
		Transition transi = new Transition();
		Place placeEntrante1=new Place(0);
		Place placeEntrante2=new Place(0);
		ArcEntrant arcEntrant1 = new ArcEntrant(1, placeEntrante1, transi);
		ArcEntrant arcEntrant2 = new ArcEntrant(1, placeEntrante2, transi);
		transi.addArcEntrant(arcEntrant1);
		transi.addArcEntrant(arcEntrant2);
		if(transi.estTirable()) {
			transi.tirer();
		}
		assertEquals(placeEntrante1.getJetons(),1);
		assertEquals(placeEntrante2.getJetons(),1);
	}
	
	@Test
	@Order(22)
	void testTM6() {
		Transition transi = new Transition();
		Place placeEntrante1=new Place(0);
		Place placeEntrante2=new Place(0);
		ArcEntrant arcEntrant1 = new ArcEntrant(3, placeEntrante1, transi);
		ArcEntrant arcEntrant2 = new ArcEntrant(2, placeEntrante2, transi);
		transi.addArcEntrant(arcEntrant1);
		transi.addArcEntrant(arcEntrant2);
		if(transi.estTirable()) {
			transi.tirer();
		}
		assertEquals(placeEntrante1.getJetons(),3);
		assertEquals(placeEntrante2.getJetons(),2);
	}
	
	//Transition activated, basic example, entering emply places and places sortantes having only 1 jeton, the weight's arcs is 1
	@Test
	@Order(23)
	void testTM7() {
		Transition transi = new Transition();
		Place placeSortante1=new Place(1);
		Place placeSortante2=new Place(1);
		Place placeEntrante1=new Place(0);
		Place placeEntrante2=new Place(0);
		ArcSortant arcSortant1 = new ArcSortant(1, placeSortante1, transi);
		ArcSortant arcSortant2 = new ArcSortant(1, placeSortante2, transi);
		ArcEntrant arcEntrant1 = new ArcEntrant(1, placeEntrante1, transi);
		ArcEntrant arcEntrant2 = new ArcEntrant(1, placeEntrante2, transi);
		transi.addToAllArcsSortants(arcSortant1);
		transi.addToAllArcsSortants(arcSortant2);
		transi.addArcEntrant(arcEntrant1);
		transi.addArcEntrant(arcEntrant2);
		if(transi.estTirable()) {
			transi.tirer();
		}
		assertEquals(placeSortante1.getJetons(),0);
		assertEquals(placeSortante2.getJetons(),0);
		assertEquals(placeEntrante1.getJetons(),1);
		assertEquals(placeEntrante2.getJetons(),1);
	}
	
	// Transition activated, Weight's arcSortant > 1
	@Test
	@Order(24)
	void testTM8() {
		Transition transi = new Transition();
		Place placeSortante1=new Place(5);
		Place placeSortante2=new Place(3);
		Place placeEntrante1=new Place(0);
		Place placeEntrante2=new Place(0);
		ArcSortant arcSortant1 = new ArcSortant(3, placeSortante1, transi);
		ArcSortant arcSortant2 = new ArcSortant(2, placeSortante2, transi);
		ArcEntrant arcEntrant1 = new ArcEntrant(1, placeEntrante1, transi);
		ArcEntrant arcEntrant2 = new ArcEntrant(1, placeEntrante2, transi);
		transi.addToAllArcsSortants(arcSortant1);
		transi.addToAllArcsSortants(arcSortant2);
		transi.addArcEntrant(arcEntrant1);
		transi.addArcEntrant(arcEntrant2);
		if(transi.estTirable()) {
			transi.tirer();
		}
		assertEquals(placeSortante1.getJetons(),2);
		assertEquals(placeSortante2.getJetons(),1);
		assertEquals(placeEntrante1.getJetons(),1);
		assertEquals(placeEntrante2.getJetons(),1);
	}
	
	//transition tirable poids arcEntrant > 1 et places vides initialement 
	@Test
	@Order(25)
	void testTM9() {
		Transition transi = new Transition();
		Place placeSortante1=new Place(1);
		Place placeSortante2=new Place(1);
		Place placeEntrante1=new Place(0);
		Place placeEntrante2=new Place(0);
		ArcSortant arcSortant1 = new ArcSortant(1, placeSortante1, transi);
		ArcSortant arcSortant2 = new ArcSortant(1, placeSortante2, transi);
		ArcEntrant arcEntrant1 = new ArcEntrant(2, placeEntrante1, transi);
		ArcEntrant arcEntrant2 = new ArcEntrant(3, placeEntrante2, transi);
		transi.addToAllArcsSortants(arcSortant1);
		transi.addToAllArcsSortants(arcSortant2);
		transi.addArcEntrant(arcEntrant1);
		transi.addArcEntrant(arcEntrant2);
		if(transi.estTirable()) {
			transi.tirer();
		}
		assertEquals(placeSortante1.getJetons(),0);
		assertEquals(placeSortante2.getJetons(),0);
		assertEquals(placeEntrante1.getJetons(),2);
		assertEquals(placeEntrante2.getJetons(),3);
	}
	//Transition tirable, poids arcEntrant > 1 et addition des jetons finaux
	@Test
	@Order(26)
	void testTM10() {
		Transition transi = new Transition();
		Place placeSortante1=new Place(1);
		Place placeSortante2=new Place(1);
		Place placeEntrante1=new Place(1);
		Place placeEntrante2=new Place(2);
		ArcSortant arcSortant1 = new ArcSortant(1, placeSortante1, transi);
		ArcSortant arcSortant2 = new ArcSortant(1, placeSortante2, transi);
		ArcEntrant arcEntrant1 = new ArcEntrant(2, placeEntrante1, transi);
		ArcEntrant arcEntrant2 = new ArcEntrant(3, placeEntrante2, transi);
		transi.addToAllArcsSortants(arcSortant1);
		transi.addToAllArcsSortants(arcSortant2);
		transi.addArcEntrant(arcEntrant1);
		transi.addArcEntrant(arcEntrant2);
		if(transi.estTirable()) {
			transi.tirer();
		}
		assertEquals(placeSortante1.getJetons(),0);
		assertEquals(placeSortante2.getJetons(),0);
		assertEquals(placeEntrante1.getJetons(),3);
		assertEquals(placeEntrante2.getJetons(),5);
	}
	//transition non tirable, weight arcSortant=1
	@Test
	@Order(27)
	void testTM11() {
		Transition transi = new Transition();
		Place placeSortante1=new Place(0);
		Place placeSortante2=new Place(0);
		Place placeEntrante1=new Place(0);
		Place placeEntrante2=new Place(1);
		ArcSortant arcSortant1 = new ArcSortant(1, placeSortante1, transi);
		ArcSortant arcSortant2 = new ArcSortant(1, placeSortante2, transi);
		ArcEntrant arcEntrant1 = new ArcEntrant(1, placeEntrante1, transi);
		ArcEntrant arcEntrant2 = new ArcEntrant(1, placeEntrante2, transi);
		transi.addToAllArcsSortants(arcSortant1);
		transi.addToAllArcsSortants(arcSortant2);
		transi.addArcEntrant(arcEntrant1);
		transi.addArcEntrant(arcEntrant2);
		assertEquals(transi.estTirable(),false);
	}
	
	//transition non tirable poids arcSortant >1
		@Test
		@Order(28)
		void testTM12() {
			Transition transi = new Transition();
			Place placeSortante1=new Place(0);
			Place placeSortante2=new Place(0);
			Place placeEntrante1=new Place(0);
			Place placeEntrante2=new Place(1);
			ArcSortant arcSortant1 = new ArcSortant(1, placeSortante1, transi);
			ArcSortant arcSortant2 = new ArcSortant(3, placeSortante2, transi);
			ArcEntrant arcEntrant1 = new ArcEntrant(1, placeEntrante1, transi);
			ArcEntrant arcEntrant2 = new ArcEntrant(1, placeEntrante2, transi);
			transi.addToAllArcsSortants(arcSortant1);
			transi.addToAllArcsSortants(arcSortant2);
			transi.addArcEntrant(arcEntrant1);
			transi.addArcEntrant(arcEntrant2);
			assertEquals(transi.estTirable(),false);
		}
	
	//transition non tirable poids arcEntrant >1
	@Test
	@Order(29)
	void testTM13() {
		Transition transi = new Transition();
		Place placeSortante1=new Place(0);
		Place placeSortante2=new Place(0);
		Place placeEntrante1=new Place(0);
		Place placeEntrante2=new Place(1);
		ArcSortant arcSortant1 = new ArcSortant(1, placeSortante1, transi);
		ArcSortant arcSortant2 = new ArcSortant(1, placeSortante2, transi);
		ArcEntrant arcEntrant1 = new ArcEntrant(2, placeEntrante1, transi);
		ArcEntrant arcEntrant2 = new ArcEntrant(1, placeEntrante2, transi);
		transi.addToAllArcsSortants(arcSortant1);
		transi.addToAllArcsSortants(arcSortant2);
		transi.addArcEntrant(arcEntrant1);
		transi.addArcEntrant(arcEntrant2);
		assertEquals(transi.estTirable(),false);
	}
	
	//activation partielle avec des arcs de poids 1
	@Test
	@Order(30)
	void testTM14() {
		Transition transi = new Transition();
		Place placeSortante1=new Place(1);
		Place placeSortante2=new Place(0);
		Place placeEntrante1=new Place(0);
		Place placeEntrante2=new Place(1);
		ArcSortant arcSortant1 = new ArcSortant(1, placeSortante1, transi);
		ArcSortant arcSortant2 = new ArcSortant(1, placeSortante2, transi);
		ArcEntrant arcEntrant1 = new ArcEntrant(1, placeEntrante1, transi);
		ArcEntrant arcEntrant2 = new ArcEntrant(1, placeEntrante2, transi);
		transi.addToAllArcsSortants(arcSortant1);
		transi.addToAllArcsSortants(arcSortant2);
		transi.addArcEntrant(arcEntrant1);
		transi.addArcEntrant(arcEntrant2);
		assertEquals(transi.estTirable(),false);
	}
	
	@Test
	@Order(31)
	void testTM15() {
		Transition transi = new Transition();
		Place placeSortante1=new Place(3);
		Place placeSortante2=new Place(1);
		Place placeEntrante1=new Place(0);
		Place placeEntrante2=new Place(1);
		ArcSortant arcSortant1 = new ArcSortant(2, placeSortante1, transi);
		ArcSortant arcSortant2 = new ArcSortant(2, placeSortante2, transi);
		ArcEntrant arcEntrant1 = new ArcEntrant(1, placeEntrante1, transi);
		ArcEntrant arcEntrant2 = new ArcEntrant(1, placeEntrante2, transi);
		transi.addToAllArcsSortants(arcSortant1);
		transi.addToAllArcsSortants(arcSortant2);
		transi.addArcEntrant(arcEntrant1);
		transi.addArcEntrant(arcEntrant2);
		assertEquals(transi.estTirable(),false);
	}
	
	@Test
	@Order(32)
	//basic test of ArcVideur where the transition is not activated
	void testTV0() {
		Transition transi = new Transition();
		Place placeSortante=new Place(0);
		ArcVideur arcVideur = new ArcVideur(placeSortante, transi);
		transi.addToAllArcsSortants(arcVideur);
		assertEquals(transi.estTirable(),false);
	}
	
	@Test
	@Order(33)
	//basic test of ArcVideur characteristic
	void testTV1() {
		Transition transi = new Transition();
		Place placeSortante=new Place(3);
		ArcVideur arcVideur = new ArcVideur(placeSortante, transi);
		transi.addToAllArcsSortants(arcVideur);
		if(transi.estTirable()) { 
			transi.tirer();
		}
		assertEquals(placeSortante.getJetons(),0);
	}
	
	@Test
	@Order(34)
	//ArcVideur not linked to the activated transition
	void testTV2() {
		Transition transi = new Transition();
		Transition transi1 = new Transition();
		Place placeSortante=new Place(3);
		ArcVideur arcVideur = new ArcVideur(placeSortante, transi1);
		transi1.addToAllArcsSortants(arcVideur);
		if(transi.estTirable()) { 
			transi.tirer();
		}
		assertEquals(placeSortante.getJetons(),3);
	}
	
	@Test
	@Order(35)
	//ArcZero linked to a not emply place, transition not activated
	void testTZ0() {
	Transition transi = new Transition();
	Place placeSortante=new Place(3);
	ArcZero arcVideur = new ArcZero( placeSortante, transi);	
	transi.addToAllArcsSortants(arcVideur); 
	assertEquals(transi.estTirable(), false); 
	}
	
	@Test
	@Order(36)
	//ArcZero linked to an emply place, transition activated
	void testTZ1() {
	Transition transi = new Transition();
	Place placeSortante=new Place(0);
	ArcZero arcVideur = new ArcZero( placeSortante, transi);	
	transi.addToAllArcsSortants(arcVideur); 
	assertEquals(transi.estTirable(), true); 
	}
	
	@Test
	@Order(37)
	//ArcZero not linked to the choosen transition
	void testTZ2() {
	Transition transi = new Transition();
	Transition transi2 = new Transition();
	Place placeSortante=new Place(0);
	Place placeEntrante=new Place(0);
	ArcZero arcVideur = new ArcZero(placeSortante, transi2);
	ArcEntrant arcEntrant = new ArcEntrant(1, placeEntrante, transi2);
	transi2.addToAllArcsSortants(arcVideur); 
	transi2.addArcEntrant(arcEntrant);
	if(transi.estTirable()) transi.tirer();
	
	assertEquals(placeEntrante.getJetons(), 0); 
	}
	
	@Test
	@Order(38)
	//ArcZero linked to the activated transition, the result is seen in placeEntrante
	void testTZ3() {
		Transition transi = new Transition();
		Place placeSortante=new Place(0);
		Place placeEntrante=new Place(0);
		ArcZero arcVideur = new ArcZero(placeSortante, transi);
		ArcEntrant arcEntrant = new ArcEntrant(1, placeEntrante, transi);
		transi.addToAllArcsSortants(arcVideur); 
		transi.addArcEntrant(arcEntrant);
		if(transi.estTirable()) transi.tirer();
		assertEquals(placeEntrante.getJetons(), 1); 
	}
	
	@Test
	@Order(39)
	//ArcZero linked to the activated transition, the result is seen in placeEntrante (without destroying the jeton already in la placeEntrante) 
	void testTZ4() {
		Transition transi = new Transition();
		Place placeSortante=new Place(0);
		Place placeEntrante=new Place(2);
		ArcZero arcVideur = new ArcZero(placeSortante, transi);
		ArcEntrant arcEntrant = new ArcEntrant(1, placeEntrante, transi);
		transi.addToAllArcsSortants(arcVideur); 
		transi.addArcEntrant(arcEntrant);
		if(transi.estTirable()) transi.tirer();
		assertEquals(placeEntrante.getJetons(), 3); 
	}
	
	@Test
	@Order(40)
	//Test ToString ArcEntrant
	void testToString1() {
		Transition transi = new Transition();
		Place placeEntrante=new Place(1);
		ArcEntrant arcEntrant = new ArcEntrant(1, placeEntrante, transi);
		transi.addArcEntrant(arcEntrant);
		assertEquals(transi.toString(), "-Transition avec 1 arc(s) entrant(s) et 0 arc(s) sortant(s). Les arcs entrants vers cette transition sont: "+"\n"+"    *Arc de poids 1 reliant la transition avec une place de poids 1"+"\n" +"Les arcs sortants de cette transition sont: "+"\n");
		}
	
	@Test
	//Test ToString with negative value in ArcSortant
	void testToString2() {
		Transition transi = new Transition();
		Place placeSortante=new Place(1);
		ArcSortant arcSortant = new ArcSortant(-1, placeSortante, transi);
		transi.addToAllArcsSortants(arcSortant);
		assertEquals(transi.toString(), "-Transition avec 0 arc(s) entrant(s) et 1 arc(s) sortant(s). Les arcs entrants vers cette transition sont: "+"\n"+"Les arcs sortants de cette transition sont: "+"\n" +"    *Arc de poids négatif -1 ce qui est impossible"+"\n");
		}
	
	@Test
	@Order(41)
	//Test Update ArcZeo
	void testUpdateArcZero() {
		Place p1 = new Place(4);
		Place p2 = new Place(0);
		Place p3 = new Place(2);
		Transition t1 = new Transition();
		Transition t2 = new Transition();
	    ArcEntrant ae1 = new ArcEntrant(1,p1,t1);
	    ArcEntrant ae2 = new ArcEntrant(2,p3,t1);
	    ArcEntrant ae3 = new ArcEntrant(2,p2,t2);
		t1.addArcEntrant(ae1);
		t1.addArcEntrant(ae2);
		t2.addArcEntrant(ae3);
		ArcZero as1 = new ArcZero(p2,t1);
		ArcZero as2 = new ArcZero(p2,t2);
		t1.addToAllArcsSortants(as1);
		t1.addToAllArcsSortants(as2);
		
		LinkedList<Transition> listeTransitions = new LinkedList<Transition>();
		listeTransitions.add(t1);
		LinkedList<Place> listePlaces = new LinkedList<Place>();
		listePlaces.add(p1);
		listePlaces.add(p2);
		LinkedList<ArcEntrant> listeArcEntrant = new LinkedList<ArcEntrant>();
		listeArcEntrant.add(ae1);
		LinkedList<Arc> listeArcSortant = new LinkedList<Arc>();
		listeArcSortant.add(as1);
		Petri petri = new Petri(listeTransitions,listeArcEntrant, listeArcSortant,listePlaces);
		ArcZero as3 = new ArcZero(p3,t1);
		petri.addToAllArcsSortants(as3);
		int taille = t1.getAllArcsSortants().size();
		t1.update(as3);
		assertEquals(taille+1, t1.getAllArcsSortants().size()); 		
	}
	
	@Test
	@Order(42)
	//Test Update Videur
	void testUpdateArcVideur() {
		Place p1 = new Place(4);
		Place p2 = new Place(0);
		Place p3 = new Place(2);
		Transition t1 = new Transition();
		Transition t2 = new Transition();
	    ArcEntrant ae1 = new ArcEntrant(1,p1,t1);
	    ArcEntrant ae2 = new ArcEntrant(2,p3,t1);
	    ArcEntrant ae3 = new ArcEntrant(2,p2,t2);
		t1.addArcEntrant(ae1);
		t1.addArcEntrant(ae2);
		t2.addArcEntrant(ae3);
		ArcVideur as1 = new ArcVideur(p2,t1);
		ArcVideur as2 = new ArcVideur(p2,t2);
		t1.addToAllArcsSortants(as1);
		t1.addToAllArcsSortants(as2);
		
		LinkedList<Transition> listeTransitions = new LinkedList<Transition>();
		listeTransitions.add(t1);
		LinkedList<Place> listePlaces = new LinkedList<Place>();
		listePlaces.add(p1);
		listePlaces.add(p2);
		LinkedList<ArcEntrant> listeArcEntrant = new LinkedList<ArcEntrant>();
		listeArcEntrant.add(ae1);
		LinkedList<Arc> listeArcSortant = new LinkedList<Arc>();
		listeArcSortant.add(as1);
		Petri petri = new Petri(listeTransitions,listeArcEntrant, listeArcSortant,listePlaces);
		ArcVideur as3 = new ArcVideur(p3,t1);
		petri.addToAllArcsSortants(as3);
		int taille = t1.getAllArcsSortants().size();
		t1.update(as3);
		assertEquals(taille+1, t1.getAllArcsSortants().size()); 		
	}
}