package filrouge_khadija_oussama;

import java.util.LinkedList;



public interface IPetri {
	
	// Getters implementation

	public LinkedList<Arc> getAllArcsSortants() ;

	public LinkedList<ArcEntrant> getArcsEntrants();

	public LinkedList<Place> getPlaces() ;

	public LinkedList<Transition> getTransitions() ;
	
	
	// Adding elements to the RdP

	public void addArcEntrant(ArcEntrant a) ;

	public void addToAllArcsSortants(ArcSortant arc) ;

	public void addToAllArcsSortants(ArcZero arc) ;

	public void addToAllArcsSortants(ArcVideur arc) ;

	public void addTransition(Transition t);
	
	public void addPlace(Place p);
	
	
	//Removing elements from the RdP

	public void removeArcEntrant(ArcEntrant a);

	public void removeArcSortant(ArcSortant a) ;
	
	public void removeArcSortant(ArcVideur a) ;
	
	public void removeArcSortant(ArcZero a) ;
	
	public void removeTransition(Transition t) ;

	public void removePlace(Place p) ;
	
	
	// Important functions for tests

	public int genererInt(int borneInf, int borneSup);

	public boolean verifyPlaceArc(LinkedList<Arc> a, LinkedList<Place> p);
	
	public boolean verifyPlace(LinkedList<? extends AbstractArc> a, LinkedList<Place> p);

	public boolean verifyTransitionArc(LinkedList<Arc> a, LinkedList<Transition> t) ;
	
	public boolean verifyTransition(LinkedList<? extends AbstractArc> a, LinkedList<Transition> t) ;

	public boolean isUnique(LinkedList<? extends AbstractArc> a);

	public boolean isUniqueArc(LinkedList<Arc> a) ;

	public boolean isConnected(LinkedList<? extends AbstractArc> a) ;

	public boolean isConnectedArc(LinkedList<Arc> a);

	public boolean exists(LinkedList<Arc> listeArc, Arc arc) ;
	
	public boolean exists(LinkedList<? extends AbstractArc> listeArc, ArcEntrant arc) ;
	
	public boolean exists(LinkedList<Place> listePlace, Place place);
	
	public boolean exists(LinkedList<Transition> listeT, Transition t) ;
	
	public Transition choixTransition(LinkedList<Transition> tabT) ;
	
	public void step() ;
	
	public void subscribe(Subscriber s);

	public void unsubscribe(Subscriber s);

	public void notifySubscribers(Arc arc) ;

	public void afficher(LinkedList<Place> places, LinkedList<Transition> transitions,
			LinkedList<ArcEntrant> arcEntrants, LinkedList<ArcSortant> arcSortants) ;
	
	}