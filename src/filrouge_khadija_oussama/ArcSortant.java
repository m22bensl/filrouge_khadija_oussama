package filrouge_khadija_oussama;

public class ArcSortant extends AbstractArc {

	
	// Constructor implementation 	
	public ArcSortant(int valeur, Place place, Transition transition) {
		super(valeur,place,transition);
	}
	
	@Override
	/*
	 * Allowing the CompareVisitor to do its job
	 * Returns true if the arc's weight is equal or less than the place's weight
	 */
	public boolean acceptCompare(ConcreteCompareVisitor visitor) {
		return visitor.visit(this);
	}

	/*
	 * Allowing the UpdateJetonsVisitor to do its job
	 */
	@Override
	public void acceptUpdateJetons(ConcreteUpdateJetonsVisitor visitor) {
		visitor.visit(this);
		
	}
}

	