package filrouge_khadija_oussama;

import java.util.LinkedList;

public class ConcreteUpdateJetonsVisitor implements UpdateJetonsVisitor {
	
	/*
	 * Implementing the visitor interface
	 * This visitor update a place's weight if a transition is picked
	 */
	
	/*
	 * This function tests if an arc exists in a list of arcs
	 */
	public boolean contains(LinkedList<Arc> listeArcs, Arc arc) {
		boolean test = false;
		int i = 0;
		int size = listeArcs.size();
		while (test == false && i < size) {
			test = arc.equals(listeArcs.get(i));
			i++;
		}
		return test;
	}

	@Override
	/*
	 * Updating the weight of a place linked to an arc of type arcEntrant after making some tests
	 */
	public void visit(ArcEntrant ae) {
		if (ae.getValeur()>=0 && ae.getTransition().getArcsEntrants().contains(ae)) {
			ae.getPlace().setJetons(ae.getPlace().getJetons() + ae.getValeur());
		};
		
	}

	@Override
	/*
	 * Updating the weight of a place linked to an arc of type arcSortant after making some tests
	 */
	public void visit(ArcSortant as) { 
		ConcreteCompareVisitor visitor = new ConcreteCompareVisitor();
		if (as.getValeur()>=0 && as.acceptCompare(visitor) && this.contains(as.getTransition().getAllArcsSortants(),as)) {
			as.getPlace().setJetons(as.getPlace().getJetons() - as.getValeur()); 
		};
		
	}

	@Override
	/*
	 * Updating the weight of a place linked to an arc of type arcVideur after making some tests
	 */
	public void visit(ArcVideur av) {
		ConcreteCompareVisitor visitor = new ConcreteCompareVisitor();
		if (av.getValeur()>=0 && av.acceptCompare(visitor) && this.contains(av.getTransition().getAllArcsSortants(),av)) {
			av.getPlace().setJetons(0); 
		};
		
	}

	@Override
	/*
	 * Updating the weight of a place linked to an arc of type arcZero after making some tests
	 */
	public void visit(ArcZero az) {
		ConcreteCompareVisitor visitor = new ConcreteCompareVisitor();
		if (az.getValeur()>=0 && az.acceptCompare(visitor) && this.contains(az.getTransition().getAllArcsSortants(),az)) {
			az.getPlace().setJetons(0); 
		};
		
	}

}
