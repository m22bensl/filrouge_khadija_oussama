package filrouge_khadija_oussama;

public interface Arc {
	
	public int getValeur();
	
	public Place getPlace() ;
	
	public Transition getTransition() ;
	
	public void setValeur(int newVal);
	
	@Override
	public boolean equals(Object o);
	
	@Override
	public String toString() ;
	
	// Strategy design pattern
	
	public boolean acceptCompare(ConcreteCompareVisitor visitor);
	
	public void acceptUpdateJetons(ConcreteUpdateJetonsVisitor visitor);
}
