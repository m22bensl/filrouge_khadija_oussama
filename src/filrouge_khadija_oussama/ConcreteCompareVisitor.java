package filrouge_khadija_oussama;

public class ConcreteCompareVisitor implements CompareVisitor{

	/*
	 * Implementing the visitor's class
	 * This visitor tests if an arc verifies the conditions necessary to pick a transition or not
	 */
	
	@Override
	/*
	 * This type of arcs doesn't interfere in the process of picking a transition
	 */
	public boolean visit(ArcEntrant ae) {
		return true;
	}

	@Override
	/*
	 * Testing if the arc's value is inferior to the corresponding place's weight
	 * The equality case is restricted to the acrVideur type
	 */
	public boolean visit(ArcSortant as) {
		return as.getValeur()<=as.getPlace().getJetons();
		
	}

	@Override
	/*
	 * The equality case
	 */
	public boolean visit(ArcVideur av) {
		return av.getValeur()==av.getPlace().getJetons();
		
	}

	
	@Override
	/*
	 * The arc is activated if it's value and the place's weight are zero
	 */
	public boolean visit(ArcZero az) {
		return az.getPlace().getJetons()==0;
		
	}

}
