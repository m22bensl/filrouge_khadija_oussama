package filrouge_khadija_oussama;

public class ArcEntrant extends AbstractArc {

	// Constructor implementation
	public ArcEntrant(int valeur, Place place, Transition transition) {
		super(valeur,place,transition);
	}
	
	
	// Visitor design pattern
	/*
	 * Allowing the CompareVisitor to do its job
	 */
	@Override
	public boolean acceptCompare(ConcreteCompareVisitor visitor) {
		return visitor.visit(this);
	}

	
	/*
	 * Allowing the UpdateJetonsVisitor to do its job
	 */
	@Override
	public void acceptUpdateJetons(ConcreteUpdateJetonsVisitor visitor) {
		visitor.visit(this);
		
	}
}

	



	