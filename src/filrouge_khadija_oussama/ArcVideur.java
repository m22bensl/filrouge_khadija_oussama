package filrouge_khadija_oussama;

public class ArcVideur extends AbstractArc{

	// Implementing the constructor
	public ArcVideur(Place place, Transition transition) {
		super(place.getJetons() == 0 ? 1 : place.getJetons(), place, transition);
	}

	/*
	 * Allowing the CompareVisitor to do its job
	 * Returns true if the arc's weight is equal to the place's weight
	 */
	@Override
	public boolean acceptCompare(ConcreteCompareVisitor visitor) {
		return visitor.visit(this);
	}

	@Override
	/*
	 * Allowing the visitor to do its job
	 */
	public void acceptUpdateJetons(ConcreteUpdateJetonsVisitor visitor) {
		visitor.visit(this);
		
	}
}
	