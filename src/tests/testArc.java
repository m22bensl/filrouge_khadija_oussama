package tests;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import filrouge_khadija_oussama.ArcEntrant;
import filrouge_khadija_oussama.ArcSortant;
import filrouge_khadija_oussama.ArcVideur;
import filrouge_khadija_oussama.ArcZero;
import filrouge_khadija_oussama.Place;
import filrouge_khadija_oussama.Transition;
import filrouge_khadija_oussama.Subscriber; 
import filrouge_khadija_oussama.Petri; 

public class testArc {
	
	@Test
	@Order(1)
	void testConstructorEntrant() {
		//Test for ArcEntrant class
		Place p = new Place(2);
		Transition t = new Transition();
		ArcEntrant arcEntrant = new ArcEntrant(-1,p,t);
		assertNotEquals(-1, arcEntrant.getValeur(),"Arc's weight is negative");
	}
	
	@Test
	@Order(2)
	void testConstructorSortant() {
		//Test for ArcSortant class
		Place p = new Place(2);
		Transition t = new Transition();
		ArcSortant arcSortant = new ArcSortant(-1,p,t);
		assertNotEquals(-1, arcSortant.getValeur(),"Arc's weight is negative");
	}
	
	@Test
	@Order(3)
	void testConstructorVideur() {
		//Test for ArcVideur class
		Place p = new Place(-2);
		Transition t = new Transition();
		ArcVideur arcVideur = new ArcVideur(p, t); 
		assertNotEquals(-2, arcVideur.getValeur(),"Arc's weight is negative");

	}
	
	@Test
	@Order(4)
	void testConstructorZero() {
		//Test for ArcZero class
		Place p = new Place(2);
		Transition t = new Transition();
		ArcZero arcZero = new ArcZero(p, t); 
		assertEquals(0, arcZero.getValeur());
	}
	
	@Test
	@Order(5)
	void testSetterZero() {
		//test for ArcZero stter
		Place p = new Place(2);
		Transition t = new Transition();
		ArcZero arcZero = new ArcZero(p, t); 
		arcZero.setValeur(6);
		assertEquals(0, arcZero.getValeur());
	}
	
	@Test
	@Order(6)
	void testSubscribe() {
		//test for the function subscribe (add a subscriber) 
		Petri petri = new Petri(); 
		Subscriber sub = new Transition(); 
		petri.subscribe(sub);
		assertEquals(petri.getSizeSubscriber(),1); 
	}
	
	@Test
	@Order(7)
	void testUnsubscribe() {
		//test for the function unsubscribe (remove a subscriber) 
		Petri petri = new Petri(); 
		Subscriber sub = new Transition();
		Subscriber sub1 = new Transition();
		petri.subscribe(sub);
		petri.subscribe(sub1);
		petri.unsubscribe(sub);
		assertEquals(petri.getSizeSubscriber(),1);
	}
	
}
