package filrouge_khadija_oussama;

import java.util.LinkedList;
import java.util.Random;

public class Petri implements IPetri{
	private LinkedList<Transition> transitions; 
	private LinkedList<ArcEntrant> arcsEntrants; 
	private LinkedList<Arc> allArcsSortants; 
	private LinkedList<Place> places;
	private LinkedList<Subscriber> subscribers;


// Constructors implementation
	/*
	 * Verifying a set of conditions 
	 */
public Petri(LinkedList<Transition> transitions, LinkedList<ArcEntrant> arcsEntrants, 
		LinkedList<Arc> allArcsSortants, LinkedList<Place> places) {
	if (this.verifyPlace(arcsEntrants,places) && this.verifyTransition(arcsEntrants, transitions)
			&& this.isUnique(arcsEntrants) && this.isConnected(arcsEntrants)){
		this.arcsEntrants = arcsEntrants;
		this.places = places;
		this.transitions = transitions;
	} else {
		this.arcsEntrants = new LinkedList<ArcEntrant>();
		this.transitions= new LinkedList<Transition>(); 
		this.places= new LinkedList<Place>();
		this.allArcsSortants = new LinkedList<Arc>(); 
	}
	if (this.verifyPlaceArc(allArcsSortants, places) && this.verifyTransitionArc(allArcsSortants, transitions)
			&& this.isUniqueArc(allArcsSortants) && this.isConnectedArc(allArcsSortants)) {
		this.allArcsSortants = allArcsSortants;
	} else {
		this.allArcsSortants = new LinkedList<Arc>();
		this.transitions= new LinkedList<Transition>(); 
		this.places= new LinkedList<Place>(); 
		this.arcsEntrants= new LinkedList<ArcEntrant>(); 
	}
	this.subscribers = new LinkedList<Subscriber>();
	this.subscribers.addAll(transitions);
}

public Petri() { 
	this.transitions= new LinkedList<Transition>(); 
	this.places= new LinkedList<Place>();
	this.allArcsSortants = new LinkedList<Arc>(); 
	this.arcsEntrants= new LinkedList<ArcEntrant>(); 
	this.subscribers = new LinkedList<Subscriber>();
}



// Getters implementation
@Override
public LinkedList<Arc> getAllArcsSortants() {
	// TODO Auto-generated method stub
	return this.allArcsSortants;
}


@Override
public LinkedList<ArcEntrant> getArcsEntrants() {
	// TODO Auto-generated method stub
	return this.arcsEntrants;
}


@Override
public LinkedList<Place> getPlaces() {
	// TODO Auto-generated method stub
	return this.places;
}


@Override
public LinkedList<Transition> getTransitions() {
	// TODO Auto-generated method stub
	return this.transitions;
}




//Adding elements to the RdP
@Override
public void addArcEntrant(ArcEntrant a) {
	this.notifySubscribers(a);
	if (!this.exists(this.getArcsEntrants(),a) && this.exists(a.getTransition().getArcsEntrants(),a)) {
		this.arcsEntrants.add(a);
	}
}


@Override
public void addToAllArcsSortants(ArcSortant arc) {
	this.notifySubscribers(arc);
	if (!this.exists(this.getAllArcsSortants(),arc) && this.exists(arc.getTransition().getAllArcsSortants(),arc)) {
		this.allArcsSortants.add(arc);	
	}
}


@Override
public void addToAllArcsSortants(ArcZero arc) {
	this.notifySubscribers(arc);
	if (!this.exists(this.getAllArcsSortants(),arc) && this.exists(arc.getTransition().getAllArcsSortants(),arc)) {
		this.allArcsSortants.add(arc);	
	}
}


@Override
public void addToAllArcsSortants(ArcVideur arc) {
	this.notifySubscribers(arc);
	if (!this.exists(this.getAllArcsSortants(),arc) && this.exists(arc.getTransition().getAllArcsSortants(),arc)) {
		this.allArcsSortants.add(arc);	
	}
}


@Override
public void addTransition(Transition t) {
	this.transitions.add(t);
}


@Override
public void addPlace(Place p) {
	if (p.getJetons()>=0) {
		this.places.add(p); 
	}
}




//Removing elements from the RdP
@Override
public void removeArcEntrant(ArcEntrant a) {
	this.arcsEntrants.remove(a);
}


@Override
public void removeArcSortant(ArcSortant a) {
	this.allArcsSortants.remove(a);
	
}

@Override
public void removeArcSortant(ArcZero a) {
	this.allArcsSortants.remove(a);
	
}

@Override
public void removeArcSortant(ArcVideur a) {
	this.allArcsSortants.remove(a);
	
}


@Override
public void removeTransition(Transition t) {
	this.transitions.remove(t);
	for (ArcEntrant ae:this.getArcsEntrants()) {
		if (ae.getTransition().equals(t)) {
			this.arcsEntrants.remove(ae);
		}
	}
	for(Arc as: this.getAllArcsSortants()) {
		if (as.getTransition().equals(t)) {
			allArcsSortants.remove(as);
		}
	}
}


@Override
public void removePlace(Place p) {
	this.places.remove(p);
	for (ArcEntrant ae:this.getArcsEntrants()) {
		if (ae.getPlace().equals(p)) {
			this.arcsEntrants.remove(ae);
		}
	}
	for(Arc as: this.getAllArcsSortants()) {
		if (as.getPlace().equals(p)) {
			allArcsSortants.remove(as);
		}
	}
} 

/*
 * Generating a random integer between borneInf et borneSup
 */
@Override
public int genererInt(int borneInf, int borneSup){
	 Random random = new Random();
	 int nb;
	 nb = borneInf+random.nextInt(borneSup-borneInf);
	 return nb;
	}


/*
 * Choosing randomly a transition from a list of transition
 */
@Override
public Transition choixTransition(LinkedList<Transition> tabT) {
	return tabT.get(genererInt(0,tabT.size())); 
}


/*
 * Searching for a transition that can be picked and picking it
 */
@Override
public void step() {
	LinkedList<Transition> listeTransition = this.getTransitions(); 
	Transition transition= choixTransition(listeTransition); 
	listeTransition.remove(transition);
	boolean test = transition.estTirable(); 
	while (test==false) {
		transition= choixTransition(listeTransition); 
		test = transition.estTirable(); 
		listeTransition.remove(transition);
	 }
	
	transition.tirer();
	
}


/*
 * Printing the elements of the RdP
 */
@Override
public void afficher(LinkedList<Place> places, LinkedList<Transition> transitions, LinkedList<ArcEntrant> arcEntrants, LinkedList<ArcSortant> arcSortants) {
	
	System.out.println("Reseau de Petri:");
	System.out.println(places.size() + " place(s)");
	System.out.println(transitions.size() + " transition(s)");
	int arcs_number = arcEntrants.size() + arcSortants.size();
	System.out.println(arcs_number + " arc(s)"+"\n" + "\n" );
	System.out.println("Liste des places:");
	for (Place p: places) {
		if (p.getJetons()>=0) {
			System.out.println(p.toString());
		} else {
			// Add +1 because of the trick we did to pass the tests in Place 
			int i = p.getJetons()+1;
			System.err.println("Place avec "+ i + " jetons ce qui est impossible");
		}
	}
	System.out.println("\n" + "\n" + "Liste des transitions");
	for (Transition t: transitions) {
		System.out.println(t.toString());
		}
		
	}



// Functions related to the Observer design pattern

/*
 * Adding a new subscriber to the list of subscribers
 */
@Override
public void subscribe(Subscriber s) {
	this.subscribers.add(s);
}


/*
 * Removing a subscriber from the list of subscribers
 */
@Override
public void unsubscribe(Subscriber s) {
	this.subscribers.remove(s);
}


/*
 * Notify the subscriber when an update in a list of arcs occurs
 * Currently the only subscribers are the transitions
 */
@Override
public void notifySubscribers(Arc arc) {
	arc.getTransition().update(arc);
}



//Important functions for the tests

/* This function verifies that arc does exist in listeArc
* Will be applied to allArcsSortants
*/
@Override
public boolean exists(LinkedList<Arc> listeArc, Arc arc) {
	boolean rslt = false;
	int i =0;
	while (i< listeArc.size() && rslt == false) {
		rslt = listeArc.get(i).equals(arc);
		i++;
	}
	return rslt;
}


/* Overloading the function exists
* Will be applied to arcsEntrants
* The use of the covariance will make it usable with other kinds of arcs
*/
@Override
public boolean exists(LinkedList<? extends AbstractArc> listeArc, ArcEntrant arc) {
	boolean rslt = false;
	int i =0;
	while (i< listeArc.size() && rslt == false) {
		rslt = listeArc.get(i).equals(arc);
		i++;
	}
	return rslt;
}

/* Overloading the function exists
* verify that place exists in listePlace
*/
@Override
public boolean exists(LinkedList<Place> listePlace, Place place) {
	boolean rslt = false;
	int i =0;
	while (i< listePlace.size() && rslt == false) {
		rslt = listePlace.get(i).equals(place);
		i++;
	}
	return rslt;
}


/* Overloading the function exists
* verify that transition exists in listeTrannsition
*/
@Override
public boolean exists(LinkedList<Transition> listeTransition, Transition transition) {
	boolean rslt = false;
	int i =0;
	while (i< listeTransition.size() && rslt == false) {
		rslt = listeTransition.get(i).equals(transition);
		i++;
	}
	return rslt;
}

/* Verify that the places related to all the defined arcs exist in the list of places
* will be applied to allArcsSortants
*/
@Override
public boolean verifyPlaceArc(LinkedList<Arc> listeArc, LinkedList<Place> listePlace) {
	boolean rslt = true;
	int i =0;
	while (i< listeArc.size() && rslt == true) {
		if (!this.exists(listePlace,listeArc.get(i).getPlace())) {
			rslt = false;
		}
		i++;
	}
	return rslt;
}


/* Same approach of verifyPlaceArc 
* will be applied to arcsEntrants
* The covariance allows the functions to be used by other types of arcs
*/
@Override
public boolean verifyPlace(LinkedList<? extends AbstractArc> a, LinkedList<Place> p) {
	boolean rslt = true;
	int i =0;
	while (i< a.size() && rslt == true) {
		if (!this.exists(p,a.get(i).getPlace())) {
			rslt = false;
		}
		i++;
	}
	return rslt;
}


/* Verify that the transitions related to all the defined arcs exist in the list of transitions
* will be applied to allArcsSortants
*/
@Override
public boolean verifyTransitionArc(LinkedList<Arc> a, LinkedList<Transition> t) {
	boolean rslt = true;
	int i =0;
	while (i< a.size() && rslt == true) {
		if (! this.exists(t, a.get(i).getTransition())) {
			rslt = false;
		}
		i++;
	}
	return rslt;
}


/* Same approach of verifyTransitionArc
* will be applied to arcsEntrants
* The covariance allows the functions to be used by other types of arcs
*/
@Override
public boolean verifyTransition(LinkedList<? extends AbstractArc> a, LinkedList<Transition> t) {
	boolean rslt = true;
	int i =0;
	while (i< a.size() && rslt == true) {
		if (! this.exists(t, a.get(i).getTransition())) {
			rslt = false;
		}
		i++;
	}
	return rslt;
}

/* Verify that the arcs in the list are mutually different
* will be applied to arcEntrants
* The convariance will allow it to be used by other kinds of arcs
*/
@Override
public boolean isUnique(LinkedList<? extends AbstractArc> listeArc) {
	boolean rslt = true;
	int i = 0;
	while (i< listeArc.size()-1 && rslt == true) {
		int j = i+1;
		while (j< listeArc.size() && rslt == true) {
			rslt = !listeArc.get(i).equals(listeArc.get(j));
			j++;
		}
		i++;
	}
	return rslt;
}

/* Same approach of isUnique but will be applied to allArcsSortants
*/
@Override
public boolean isUniqueArc(LinkedList<Arc> listeArc) {
	boolean rslt = true;
	int i = 0;
	while (i< listeArc.size()-1 && rslt == true) {
		int j = i+1;
		while (j< listeArc.size() && rslt == true) {
			rslt = !listeArc.get(i).equals(listeArc.get(j));
			j++;
		}
		i++;
	}
	return rslt;
}


/* Verify that each arc is in the list of arcs of its corresponding transition
* The convariance will allow it to be used by other kinds of arcs
* will be applied to arcsEntrants
*/
@Override
public boolean isConnected(LinkedList<? extends AbstractArc> listeArc) {
	boolean rslt = true;
	int i =0;
	while (i< listeArc.size() && rslt == true) {
		ArcEntrant ae = (ArcEntrant) (listeArc.get(i));
		rslt = this.exists(ae.getTransition().getArcsEntrants(),ae);
		i++;
	}
	return rslt;
}


/*
* Same approach as isConnected but will be applied to allArcsSortants
*/
@Override
public boolean isConnectedArc(LinkedList<Arc> listeArc) {
	boolean rslt = true;
	int i =0;
	while (i< listeArc.size() && rslt == true) {
		Arc arc = listeArc.get(i);
		rslt = this.exists(arc.getTransition().getAllArcsSortants(),arc);
		i++;
	}
	return rslt;
}

/*
* only used in test, to test the functions subscribe and unsubscribe
*/
public int getSizeSubscriber() {
	return this.subscribers.size(); 
}

}


