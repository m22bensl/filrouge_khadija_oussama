package filrouge_khadija_oussama;

public class main {

	public static void main(String[] args) {
		// Creation of RdP
		Petri petri = new Petri();
		
		// Creation of places
		Place p1 = new Place(4);
		Place p2 = new Place(0);
		petri.addPlace(p1);
		petri.addPlace(p2);
		
		// Creation of transitions
		Transition t1 = new Transition();
		petri.addTransition(t1);
		
		// Creation of "Arcs Entrants"
		ArcEntrant ae1 = new ArcEntrant(1,p1,t1);
		petri.addArcEntrant(ae1);
		t1.addArcEntrant(ae1);
		
		// Creation of "Arcs Sortants"
		ArcSortant as1 = new ArcSortant(1,p2,t1);
		petri.addArcSortant(as1);
		t1.addArcSortant(as1);
		
		// Display of RdP
		System.out.println("Reseau de Petri:");
		System.out.println(petri.getPlaces().size() + " place(s)");
		System.out.println(petri.getTransitions().size() + " transition(s)");
		int arcs_number = petri.getArcEntrants().size() + petri.getArcSortants().size();
		System.out.println(arcs_number + " arc(s)"+"\n" + "\n" );
		System.out.println("Liste des places:");
		for (Place p: petri.getPlaces()) {
			System.out.println(p.toString());
		}
		System.out.println("\n" + "\n" + "Liste des transitions");
		for (Transition t: petri.getTransitions()) {
			System.out.println(t.toString());
		}
	}
}
