package filrouge_khadija_oussama;

public interface UpdateJetonsVisitor {
	
	public void visit(ArcEntrant ae);
	
	public void visit(ArcSortant as);
	
	public void visit(ArcVideur av);
	
	public void visit(ArcZero az);
	

}