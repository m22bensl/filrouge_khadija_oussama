package filrouge_khadija_oussama;

public interface CompareVisitor {
	
	public boolean visit(ArcEntrant ae);
	
	public boolean visit(ArcSortant as);
	
	public boolean visit(ArcVideur av);
	
	public boolean visit(ArcZero az);

}